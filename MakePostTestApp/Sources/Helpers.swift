//
//  Helpers.swift
//  MakePostTestApp
//
//  Created by Lera Mozgovaya on 2/14/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import Foundation
import PhotosUI

class Helpers {
    
    static func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 500, height: 500), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    static func removeHomeDirectoryPath(_ fullFilePath: String) -> String? {
        
        let filePathWithoutHomePrefix = fullFilePath.replacingOccurrences(of: NSTemporaryDirectory(), with: "")
        
        return filePathWithoutHomePrefix
    }
    
    static func libraryPath(_ relPath: String) -> String? {
        
        let url = URL(string: NSTemporaryDirectory())
        
        let path = url!.appendingPathComponent(relPath).path
        
        return path
    }
    
    static func dateFromTimestamp(_ timestamp: TimeInterval, format: String? = nil) -> String {
        
        let date = Date(timeIntervalSince1970: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        
        if let format = format {
            dateFormatter.dateFormat = format
        }
        else {
            dateFormatter.dateFormat = "MMMM dd, YYYY"
        }
        
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
}
