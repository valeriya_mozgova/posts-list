//
//  MakePostHeaderWithCollection.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

enum PhotoItemType {
    
    case photoForUploadObject
    case photoDisplayModel
    case tripAvatar
}

protocol PhotoItem {
    
    var photoItemType: PhotoItemType { get }
}

class MakePostHeaderWithCollection: UITableViewHeaderFooterView {

    //MARK: - Outlets
    @IBOutlet weak var addPhotoBtn: UIButton!
    @IBOutlet weak var addPhotoPlainBtn: UIButton!
    @IBOutlet weak var photosCollection: UICollectionView! {
        didSet {
            photosCollection.delegate   = self
            photosCollection.dataSource = self
        }
    }
    
    //MARK: - Properties
    lazy var photoPickerService: PhotosPickService = {
        return PhotosPickService(maxSelectionCount: 14)
    }()
    
    weak var output: MakePostHeaderWithCollectionOutput?
    var photosDisplayModel: HeaderWithCollectionDisplayModel!
    private var selectedPhoto: PhotoForUploadObject?
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    
    private var selectedAction: (UIAlertAction)?
    
    //MARK: - Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        photosCollection.addGestureRecognizer(longPressGesture)
        photoPickerService.output = self
    }
    
    //MARK: - Public
    func configure(with displayModel: HeaderWithCollectionDisplayModel) {
        
        photosDisplayModel = displayModel
        
        photosCollection.isHidden = (displayModel.images.count > 0) ? false : true
        addPhotoPlainBtn.isHidden = (displayModel.images.count > 0) ? true  : false
        
        photosCollection.reloadData()
    }
    
    func saveInitialPhotoObject(photoObject: PhotoForUploadObject) {
        appendPhoto(photoObject)
    }
    
    //MARK: - Actions
    
    @IBAction func didClickAddPhoto(_ sender: UIButton) {
        
        self.output?.didAddPhoto(withPicker: photoPickerService, selectedAction: { [weak self] selectedAction in
         
            self?.selectedAction = selectedAction
        })
    }
    
    //MARK: - Gestures
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        
        switch(gesture.state) {
        case .began:
            guard let selectedIndexPath = photosCollection.indexPathForItem(at: gesture.location(in: photosCollection)) else { break }
            
            photosCollection.beginInteractiveMovementForItem(at: selectedIndexPath)
            
        case .changed:
            photosCollection.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
            
        case .ended:
            photosCollection.endInteractiveMovement()
            photosCollection.reloadData()
            
        default:
            photosCollection.cancelInteractiveMovement()
        }
    }
}

extension MakePostHeaderWithCollection: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let photo = photosDisplayModel.images[indexPath.row]
        
        let alertActions = updateAlertActions(for: photo)
        
        self.output?.didSelectPhotoWithActions(alertActions, selectedAction: { [weak self] (action) in
            
            self?.handleSelectedPhotoAction(photo: photo, action: action)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        if indexPath.item == 0 {
            return CGSize(width: 68, height: 68)
        }
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
       
        if indexPath.row == 0 {
            return false
        }
        
        return photosDisplayModel.canMoveItems
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let movingItem = self.photosDisplayModel.images[sourceIndexPath.item]
        
        self.photosDisplayModel.images.remove(at: sourceIndexPath.item)
        self.photosDisplayModel.images.insert(movingItem, at: destinationIndexPath.item)
    }
}

extension MakePostHeaderWithCollection: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return photosDisplayModel.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = PhotoCollectionCell.cell(collectionView: collectionView, indexPath: indexPath) as! PhotoCollectionCell
             
        cell.configure(with: photosDisplayModel.images[indexPath.item])
        return cell
    }
}

extension MakePostHeaderWithCollection: PhotosPickerServiceOutput {
    
    func showMessage(_ message: String) {
        output?.showMessage(message)
    }
    
    func didCancelCamera() {
        return
    }
    
    func updateHeader(withPhoto photoObject: PhotoForUploadObject) {
         appendPhoto(photoObject)
    }
}

//MARK: Private
extension MakePostHeaderWithCollection {

    private func handleSelectedPhotoAction(photo: PhotoItem, action: UIAlertAction) {
        
        switch photo.photoItemType {
        case .photoForUploadObject:
            guard let photo = photo as? PhotoForUploadObject else { return }
            handleSelectedPhotoForUploadAction(photo, action: action)
        case .photoDisplayModel: break
        case .tripAvatar:
            break
        }
    }
    
    private func appendPhoto(_ photoObject: PhotoForUploadObject) {
        
        if photosDisplayModel.images.contains(where: { ($0.photoItemType == .photoForUploadObject) && ($0 as! PhotoForUploadObject).imageData == photoObject.imageData
        }) == false {
            if photosDisplayModel.images.count < 15 {
                self.addPhoto(photoObject)
            }
            else {
                showMessage("You  can not exceed 15 photos per single Post")
            }
        }
        else {
            showMessage("You have already appended this photo(s)")
        }
    }
    
    private func addPhoto(_ photo: PhotoItem) {
    
        photosCollection.numberOfItems(inSection: 0)
        
        if let selectedAction = selectedAction, selectedAction.title == "Upload avatar" {
            
            photosDisplayModel.images.insert(photo, at: 0)
            photosCollection.insertItems(at: [IndexPath(item: 0, section: 0)])
        }
        else {
            photosDisplayModel.images.append(photo)
            photosCollection.insertItems(at: [IndexPath(item: self.photosDisplayModel.images.count - 1, section: 0)])
        }
        
        photosCollection.isHidden = false
        addPhotoPlainBtn.isHidden = true
        addPhotoBtn.setImage(UIImage(named: "buttonAddPhoto"), for: .normal)
    }
    
    private func deletePhoto(_ photo: PhotoItem) {
        
        switch photo.photoItemType {
        case .photoDisplayModel: break
        case .photoForUploadObject:
            
            let imgs = photosDisplayModel.images as! [PhotoForUploadObject]
            
            let idxToRemove = imgs.index { $0.imageData == (photo as! PhotoForUploadObject).imageData }
            if let idx = idxToRemove {
                photosDisplayModel.images.remove(at: idx)
                photosCollection.deleteItems(at: [IndexPath(item: idx, section: 0)])
            }
            
        case .tripAvatar:
            break
        }
    }
    
    private func updateAlertActions(for photo: PhotoItem) -> [(String, UIAlertAction.Style)] {
        
        var alertActions = [(String, UIAlertAction.Style)]()
        
        switch photosDisplayModel.headerSource {
        case .createPlace: break
        case .editPost:
            
            alertActions = [("Set as cover post photo", .default),
                            ("Delete photo", .destructive),
                            ("Cancel", .cancel)]
            
            if photo.photoItemType == .photoForUploadObject {
                alertActions.remove(at: 0)
            }
            
        case .makePost:
            
            guard photo is PhotoForUploadObject else { return [] }
            
            alertActions = [("Set as cover trip photo", .default),
                            ("Edit photo", .default)]
            

            let deleteAction = ("Delete photo", UIAlertAction.Style.destructive)
            alertActions.append(deleteAction)
            let cancelAction = ("Cancel", UIAlertAction.Style.cancel)
            alertActions.append(cancelAction)
            

        case .editTrip: break
            
        }
        return alertActions
    }
    
    func handleSelectedPhotoForUploadAction(_ photo: PhotoForUploadObject, action: UIAlertAction) {
        
        switch action.title {

        case "Delete photo"?:
            
            photoPickerService.removePhotoPhysically(pathToResource: photo.pathToImage, completion: {
                deletePhoto(photo)
                
                photosDisplayModel.images.removeAll(where: { ($0.photoItemType == .photoForUploadObject) && ($0 as! PhotoForUploadObject).imageData == photo.imageData
                })
            })
            
        default:
            break
        }
    }
    
    private func reloadCells(at indexPaths: [IndexPath]) {
        photosCollection.reloadItems(at: indexPaths)
    }
}

