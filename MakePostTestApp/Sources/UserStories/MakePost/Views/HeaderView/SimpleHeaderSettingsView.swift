//
//  SimpleHeaderSettingsView.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 7/26/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class SimpleHeaderSettingsView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    func configureHeaderObject(_ headerObj: SimpleSettingsHeaderObject) {
        
        headerTitleLabel.text =  headerObj.title
        
        if let font = headerObj.font {
            headerTitleLabel.font = font
        }
        else {
            headerTitleLabel.font = UIFont(name: "SFProText-Medium", size: 12)
        }
        headerTitleLabel.textColor = .gray
    }
}
