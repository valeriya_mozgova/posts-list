//
//  SimpleSettingsHeaderObject.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 7/26/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation
import UIKit

struct SimpleSettingsHeaderObject {
    
    var title: String
    var font: UIFont?
    var headerHeight: CGFloat?
    
    init(title: String, headerHeight: CGFloat? = nil, font: UIFont? = nil) {
        self.title = title
        self.font = font
    }
}

extension SimpleSettingsHeaderObject: MakePostHeaderItem {
    
    var type: MakePostHeaderItemType {
        return .simpleTextType
    }
}

