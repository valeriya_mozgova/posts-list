//
//  MakePostEnums.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation

enum FullPlaceType: String {
    case geoplace = "GEOPLACE"
    case business = "BUSINESS"
}

enum MakePostViewModelItemType {
    case notateCell
    case cellWithPlaceholder
    case audioCell
    case confidentialityLevel
    case placeCell
    case buttonCell
}

enum MakePostSectionType: Int {
    case title = 0
    case notes = 1
    case audioDescription = 2
    case confidentialityLevel = 3
    case areUIn = 4
    case addPlace = 5
    case sharing = 6
    case noCells = 7
}

enum PrivacyLevel: Int {
    
    case publicLevel = 0,
    restrictedLevel,
    privateLevel
    
    var privacyLevel: String {
        let privacyTypes = [
            "PUBLIC",
            "RESTRICTED",
            "PRIVATE"]
        return privacyTypes[rawValue]
    }
}

enum MakePostHeaderItemType {
    case addPhotosType
    case simpleTextType
}

var appHasMicAccess = true

enum AudioStatus: Int, CustomStringConvertible {
    
    case stopped = 0,
    playing,
    recording,
    paused
    
    var audioName: String {
        let audioNames = [
            "Audio:Stopped",
            "Audio:Playing",
            "Audio:Recording",
            "Audio:Paused"]
        return audioNames[rawValue]
    }
    
    var description: String {
        return audioName
    }
}

enum MediaResourceType {
    case photoMediaType
    case audioMediaType
}
