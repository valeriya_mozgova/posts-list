//
//  BaseTableViewCell.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 11/6/17.
//  Copyright © 2017 Traveleeg Inc. All rights reserved.
//

import UIKit

class BaseTableViewCell : UITableViewCell {
    
    static func cell(tableView: UITableView) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: self))
        
        guard cell != nil else {
            let nib = UINib(nibName: String(describing: self), bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: String(describing: self))
            return tableView.dequeueReusableCell(withIdentifier: String(describing: self))!;
        }
        
        return cell!
    }
}

class BaseCollectionViewCell: UICollectionViewCell {
    
    static func cell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        
        let nib = UINib(nibName: String(describing: self), bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: String(describing: self))
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: self), for: indexPath);
    }
}
