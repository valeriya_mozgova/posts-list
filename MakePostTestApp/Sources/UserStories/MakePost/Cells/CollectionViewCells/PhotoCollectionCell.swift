//
//  PhotoCollectionCell.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class PhotoCollectionCell: BaseCollectionViewCell {

    @IBOutlet weak var photoImgView: UIImageView!
    
    func configure(with image: PhotoItem) {
        
        switch image.photoItemType {
        case .photoDisplayModel, .tripAvatar: break
        case .photoForUploadObject:
            let photo = image as! PhotoForUploadObject
            photoImgView.image = photo.photo
        }
    }
}
