//
//  ConfLevelCell.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class ConfLevelCell: BaseTableViewCell {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var publishAsLabel: UILabel!
    
    @IBOutlet weak var segmentedControlVerticalConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var segmentedControlToPublishTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var segmentedControlBottomConstraint: NSLayoutConstraint!
    
    
    var cellObject: ConfLevelCellObject!
    
    func configure(with confCellObj: ConfLevelCellObject, titlesArray: [String]) {
        cellObject = confCellObj
    
        segmentedControl.removeAllSegments()
        
        for (idx, item) in titlesArray.enumerated() {
            segmentedControl.insertSegment(withTitle: item, at: idx, animated: true)
        }
        
        segmentedControl.selectedSegmentIndex = confCellObj.privacyLevel.rawValue
    
        if let isVisiblePublish = confCellObj.isVisiblePublish {
    
            publishAsLabel.isHidden = !isVisiblePublish
            segmentedControlVerticalConstaint.isActive = !isVisiblePublish
            segmentedControlToPublishTopConstraint.isActive = isVisiblePublish
            
            segmentedControlBottomConstraint.isActive = isVisiblePublish
        }
    }
    
    @IBAction func didChangePrivacyLevel(_ sender: UISegmentedControl) {
        cellObject.privacyLevel = PrivacyLevel(rawValue: sender.selectedSegmentIndex)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let isVisiblePublish = cellObject.isVisiblePublish {
            
            publishAsLabel.isHidden = !isVisiblePublish
            segmentedControlVerticalConstaint.isActive = !isVisiblePublish
            segmentedControlToPublishTopConstraint.isActive = isVisiblePublish
            
            segmentedControlBottomConstraint.isActive = isVisiblePublish
        }
    }
}
