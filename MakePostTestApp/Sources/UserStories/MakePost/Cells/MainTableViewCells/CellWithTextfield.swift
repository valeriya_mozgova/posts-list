//
//  MkePostCellWithTextfield.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class CellWithTextfield: BaseTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var cellObject: DescriptionWithPlaceholderCellObject! 
        
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
    }

    func configure(with object: DescriptionWithPlaceholderCellObject) {
        
        cellObject = object
        titleLabel.text = object.title
        textField.placeholder = object.placeholderText
        textField.text = object.text
        
        textField.keyboardType = .default
        
        textField.isUserInteractionEnabled = cellObject.enabledTextField
    }
}

extension CellWithTextfield: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        cellObject.text = textField.text!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        if let text = text {
            cellObject.text = text
        }
    
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     
        textField.resignFirstResponder()
    
        return true
    }
}
