//
//  NotateCell.swift
//  Enguide
//
//  Created by OnSightTeam on 10/16/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class NotateCell: BaseTableViewCell  {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var txtView: UITextView!
    
    weak var output: NotateCellOutput?
    
    private var object: NotateCellObject?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtView.delegate = self

    }

    func configure(with object: NotateCellObject) {
        
        self.object = object
        
        title.text = object.title
        txtView.text = object.text
    }
}

extension NotateCell: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        guard let obj = self.object else { return }
        
        obj.text = textView.text
        
        let height = txtView.newHeight(withBaseHeight: 44)
      
        output?.changeSizeOfCell(height)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return true
        }
        return true
    }
}
