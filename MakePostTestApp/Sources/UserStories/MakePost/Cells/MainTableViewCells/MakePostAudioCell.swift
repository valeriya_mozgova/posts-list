//
//  MakePostAudioCell.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class MakePostAudioCell: BaseTableViewCell {

    @IBOutlet weak var audioDescriptionView: AudioDescriptionView! {
        didSet {
            audioDescriptionView.output = self
        }
    }
    
    //properties
    weak var output: AudioDescriptionCellOutput?
    
    //MARK: Public
    func configure(with audioCellObj: AudioDescriptionCellObject) {
        audioDescriptionView.audioDescriptionObject = audioCellObj
    }
}

extension MakePostAudioCell: AudioDescriptionCellOutput {
    
    func openAudioActionsMenu(actions: [(String, UIAlertAction.Style)], selectedAction: @escaping (UIAlertAction) -> Void) {
        output?.openAudioActionsMenu(actions: actions, selectedAction: selectedAction)
    }
    
    func openAudioRecorder(with descriptionObject: AudioDescriptionCellObject) {
        output?.openAudioRecorder(with: descriptionObject)
    }
    
    func showAlert(_ message: String) {
        output?.showAlert(message)
    }
    
    func removeAudioDescription(_ audioDesc: AudioDescriptionCellObject) {
        output?.removeAudioDescription(audioDesc)
    }
}
