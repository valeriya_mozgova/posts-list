//
//  MakePostCellFactory.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/26/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation
import UIKit

class MakePostCellFactory {
    func createCell(forTable tableView: UITableView, withContent content: MakePostViewModelItem, at indexPath: IndexPath, viewModel: MakePostViewModel) -> UITableViewCell {
        
        switch content.cellType {
        case .cellWithPlaceholder:
            let cellObject = content  as! DescriptionWithPlaceholderCellObject
            let cell = CellWithTextfield.cell(tableView: tableView) as! CellWithTextfield
            cell.configure(with: cellObject)
            return cell
            
        case .notateCell:
            
            let cellObject = content as! NotateCellObject
            let cell = NotateCell.cell(tableView: tableView) as! NotateCell
            cell.configure(with: cellObject)
            cell.output = viewModel
            return cell
            
        case .audioCell:
            let cellObject = content as! AudioDescriptionCellObject
            let cell = MakePostAudioCell.cell(tableView: tableView) as! MakePostAudioCell
            cell.configure(with: cellObject)
            cell.output = viewModel
            return cell
            
        case .confidentialityLevel:
            let cellObj = content as! ConfLevelCellObject
            let cell = ConfLevelCell.cell(tableView: tableView) as! ConfLevelCell
            cell.configure(with: cellObj, titlesArray: ["Public", "Restricted", "Private"])
            return cell
        
        case .placeCell: return UITableViewCell()
        case .buttonCell: return UITableViewCell()
        }
    }
}
