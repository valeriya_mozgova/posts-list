//
//  MakePostProtocols.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation
import UIKit

// ViewModel
protocol MakePostViewModelOutput: AudioDescriptionCellOutput, MakePostHeaderWithCollectionOutput {
    
    func reloadCells(at indexPaths: [IndexPath])
    func updateTableSize()
    func didFinishCustomizeHeader(_ header: MakePostHeaderWithCollection)
    func cellAtIndexPath(_ indexPath: IndexPath) -> UITableViewCell?
}

protocol MakePostViewModelDataInput: class {
    func sendPost(_ post: MakePostModel)
}

// Cells
protocol MakePostHeaderWithCollectionOutput: class {
    
    func didAddPhoto(withPicker picker: PhotosPickService, selectedAction: ((UIAlertAction) -> Void)?)
    func didSelectPhotoWithActions(_ actions: [(String, UIAlertAction.Style)], selectedAction: @escaping (UIAlertAction) -> Void)
    func showMessage(_ msg: String)
}

extension MakePostHeaderWithCollectionOutput {
    
    func didAddPhoto(withPicker picker: PhotosPickService, selectedAction: ((UIAlertAction) -> Void)? = nil) {
        didAddPhoto(withPicker: picker, selectedAction: selectedAction)
    }
}

protocol AudioDescriptionCellOutput: class {
    func openAudioRecorder(with descriptionObject: AudioDescriptionCellObject)
    func openAudioActionsMenu(actions: [(String, UIAlertAction.Style)], selectedAction: @escaping (UIAlertAction) -> Void)
    func showAlert(_ message: String)
    func removeAudioDescription(_ audioDesc: AudioDescriptionCellObject)
}

extension AudioDescriptionCellOutput {
    
    func openAudioRecorder(with descriptionObject: AudioDescriptionCellObject) { }
    func openAudioActionsMenu(actions: [(String, UIAlertAction.Style)], selectedAction:  @escaping (UIAlertAction) -> Void) { }
    func removeAudioDescription(_ audioDesc: AudioDescriptionCellObject) { }
}

protocol NotateCellOutput: class {
    func changeSizeOfCell(_ size: CGFloat)
}

// RecorderController
protocol AudioRecorderControllerOutput: class {
    func didSaveRecordedAudio(_ audioDescription: AudioDescriptionCellObject)
    func removeBlurredBackgroundView()
}

// Audio services
protocol AudioRecorderServiceOutput: class {
    func updateCurrentTime(_ time: String)
    func didRecieveSavingError(_ error: String)
}

protocol AudioPlayerServiceOutput: class {
    func updatePlayerUI(_ audioDescription: AudioDescriptionCellObject, playingTime: TimeInterval)
}

// ViewModel items
protocol MakePostHeaderItem {
    
    var type: MakePostHeaderItemType { get }
}

protocol MakePostViewModelItem {
    
    var cellType: MakePostViewModelItemType { get }
    var rowHeight: CGFloat { get }
}

extension MakePostViewModelItem {
    
    var rowCount: Int {
        return 1
    }
}
