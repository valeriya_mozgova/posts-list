//
//  MakePostViewController.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Photos

class MakePostViewController: UIViewController {

    // outlets
    @IBOutlet weak var tableView: UITableView!

    // properties
    private var photoForUpload: PhotoForUploadObject?
    
    // lazy vars
    lazy var viewModel: MakePostViewModel = {
        return MakePostViewModel(output: self, dataInput: self.dataProvider)
    }()
    
    lazy var dataProvider: MakePostDataProvider = {
        return MakePostDataProvider()
    }()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        binding()
        registerXibs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: Actions
    @IBAction func didClickBack(_ sender: UIBarButtonItem) {
        
        showAlert(title: "Do you really want to quit without saving post?", message: "", actions: [("Cancel", .default), ("Quit", .cancel)], style: .alert) { (action) in
            
            switch action.title {
            case "Cancel":
                break
            case "Quit":
                self.removePhotosOnExit()
                self.navigationController?.popViewController(animated: true)
            default:
                break
            }
        }
    }
    
    @IBAction func didClickSave(_ sender: UIBarButtonItem) {
        
        viewModel.createPost()
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Public
    func configureWithTakenPhoto(_ photo: PhotoForUploadObject) {
        photoForUpload = photo
    }
    
    //MARK: Touches
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: Private
    private func binding() {
        tableView.delegate      = viewModel
        tableView.dataSource    = viewModel
    }
    
    private func registerXibs() {
    
        let nib = UINib(nibName: kHeaderWithCollectionXibName, bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: kHeaderWithCollectionReuseID)
    }
    
    private func removePhotosOnExit() {
        
        guard let photos = viewModel.makePostModel.photos, photos.count > 0 else { return }
        
        for photo in photos {
            PhotosPickService(maxSelectionCount: 0).removePhotoPhysically(pathToResource: photo.pathToImage, completion: {})
        }
    }
    
    fileprivate func openCamera(withService service: PhotosPickService) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = service
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false

            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    fileprivate func openCameraRoll(withService service: PhotosPickService) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {

            let picker = AssetsPickerViewController()
            picker.pickerDelegate = service
            present(picker, animated: true, completion: nil)
        }
    }
    
    func overlayBlurredBackgroundView() {
        
        let blurredBackgroundView = UIView()

        blurredBackgroundView.tag = 100
        blurredBackgroundView.frame = CGRect(x: view.bounds.origin.x, y: view.bounds.origin.y, width: view.bounds.width, height: view.bounds.height + (navigationController?.navigationBar.frame.height)! + UIApplication.shared.statusBarFrame.height)
        blurredBackgroundView.backgroundColor = .black
        blurredBackgroundView.alpha = 0.9
        navigationController?.view.addSubview(blurredBackgroundView)
    }
}

extension MakePostViewController: MakePostViewModelOutput {
    
    func cellAtIndexPath(_ indexPath: IndexPath) -> UITableViewCell? {
        return tableView.cellForRow(at: indexPath)
    }
    
    func showMessage(_ msg: String) {
        showAlert(message: msg, style: .alert)
    }

    func showAlert(_ message: String) {
        showAlert(message: message, style: .alert)
    }

    func openAudioActionsMenu(actions: [(String, UIAlertAction.Style)], selectedAction:  @escaping (UIAlertAction) -> Void) {
        
        showAlert(actions: actions, style: .actionSheet, completion: selectedAction)
    }

    func didSelectPhotoWithActions(_ actions: [(String, UIAlertAction.Style)], selectedAction: @escaping (UIAlertAction) -> Void) {
        
        showAlert(actions: actions, style: .actionSheet, completion: selectedAction)
    }

    func didAddPhoto(withPicker picker: PhotosPickService, selectedAction: ((UIAlertAction) -> Void)?) {
        let cameraAction = ("Camera", UIAlertAction.Style.default)
        let cameraRollAction = ("Camera Roll", UIAlertAction.Style.default)
        let cancelAction = ("Cancel", UIAlertAction.Style.cancel)
        
        showAlert(actions: [cameraAction, cameraRollAction, cancelAction], style: .actionSheet) { (action) in
            switch action.title {
            case "Camera"?:
                self.openCamera(withService: picker)
            case "Camera Roll"?:
                self.openCameraRoll(withService: picker)
            case "Cancel"?:
                self.presentedViewController?.dismiss(animated: true, completion: nil)
            default:
                break
            }
        }
    }
    
    func reloadCells(at indexPaths: [IndexPath]) {
        
        UIView.performWithoutAnimation {
            
            self.tableView.layoutIfNeeded()
            self.tableView.reloadRows(at: indexPaths, with: .none)
        }
    }
    
    func updateTableSize() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func didFinishCustomizeHeader(_ header: MakePostHeaderWithCollection) {
        guard let photoObj = photoForUpload else { return }
        header.saveInitialPhotoObject(photoObject: photoObj)
    }
    
    func playAudioTrack(_ audioDescription: AudioDescriptionCellObject) {
        view.endEditing(true)
    }
    
    func openAudioRecorder(with descriptionObject: AudioDescriptionCellObject) {
        
        self.openRecorder(with: descriptionObject)
    }
    
    fileprivate func openRecorder(with descriptionObject: AudioDescriptionCellObject) {
        
        view.endEditing(true)
    
        let vc: AudioRecorderController = AudioRecorderController.instantiate(appStoryboard: .audio)
        
        vc.configure(with: descriptionObject)
        vc.output = self
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        
        self.overlayBlurredBackgroundView()
    }
    
    func reloadView() {
        tableView.reloadData()
    }
    
    func cellAtIndexPath(at: [IndexPath]) -> UITableViewCell? {
        return tableView.cellForRow(at: at.first!)
    }
}

extension MakePostViewController: AudioRecorderControllerOutput {
  
    func didSaveRecordedAudio(_ audioDescription: AudioDescriptionCellObject) {
        viewModel.saveAudioDescription(audioDescription)
    }

    func removeBlurredBackgroundView() {
        
        for subview in navigationController!.view.subviews {
            if subview.isKind(of: UIView.self) && subview.tag == 100 {
                subview.removeFromSuperview()
            }
        }
    }
}
