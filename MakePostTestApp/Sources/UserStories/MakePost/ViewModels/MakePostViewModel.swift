 //
//  MakePostViewModel.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class MakePostViewModel: NSObject {

    // properties
    var makePostContentConfig: MakePostContentConfiguration!
    
    var expandingCellHeight: CGFloat = 44
    
    weak var output: MakePostViewModelOutput?
    unowned let dataInput: MakePostViewModelDataInput
    
    var defaultPrivacyLevel: PrivacyLevel {
        let defaultPrivacyLevel = PrivacyLevel(rawValue: 0)!
        return defaultPrivacyLevel
    }

    var makePostModel: MakePostModel!
    
    // MARK: Init
    init(output: MakePostViewModelOutput, dataInput: MakePostViewModelDataInput) {
        self.output    = output
        self.dataInput = dataInput
        
        super.init()
        
        makePostContentConfig = MakePostContentConfiguration.makePostConfig(defaultPrivacyLevel)
        
        let images = (makePostContentConfig.sections[0].header as! HeaderWithCollectionDisplayModel).images as! [PhotoForUploadObject]
        
        makePostModel = MakePostModel(clientId: NSString().randomString(length: 10), title: nil, photos: images, note: nil, audioDescription: nil, confLevel: defaultPrivacyLevel)
    }
    
    //MARK: Public
    func saveAudioDescription(_ audioDescription: AudioDescriptionCellObject) {
        
        audioDescription.shouldShowMenu = true
        let sectionIdx: MakePostSectionType = .audioDescription
        makePostContentConfig.sections[sectionIdx.rawValue].cell = audioDescription

        if let cell: MakePostAudioCell = output?.cellAtIndexPath(IndexPath(row: 0, section: sectionIdx.rawValue)) as? MakePostAudioCell {
            cell.configure(with: audioDescription)
        }
    }
}

extension MakePostViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        guard let headerItem = makePostContentConfig.sections[section].header else { return nil }
        
        switch headerItem.type {
        case .addPhotosType:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: kHeaderWithCollectionReuseID) as? MakePostHeaderWithCollection
            header?.configure(with: headerItem as! HeaderWithCollectionDisplayModel)
            header?.output = self
            
            output?.didFinishCustomizeHeader(header!)
            
            return header
            
        case .simpleTextType:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: SimpleHeaderSettingsView.self)) as? SimpleHeaderSettingsView
            header?.configureHeaderObject(headerItem as! SimpleSettingsHeaderObject)
            
            return header
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return makePostContentConfig.sections[section].headerHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath == IndexPath(row: 0, section: 1) {
                
            return expandingCellHeight
        }
        
        return UITableView.automaticDimension
    }
}

extension MakePostViewModel: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return makePostContentConfig.sections[section].cell.rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellFactory = MakePostCellFactory()
                
        let cell = cellFactory.createCell(forTable: tableView,
                               withContent: makePostContentConfig.sections[indexPath.section].cell,
                               at: indexPath,
                               viewModel: self)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return makePostContentConfig.sections.count
    }
}

 extension MakePostViewModel: MakePostHeaderWithCollectionOutput {
    func showMessage(_ msg: String) {
        
    }

    func didSelectPhotoWithActions(_ actions: [(String, UIAlertAction.Style)], selectedAction: @escaping (UIAlertAction) -> Void) {
        self.output?.didSelectPhotoWithActions(actions, selectedAction: selectedAction)
    }
    
    func didAddPhoto(withPicker picker: PhotosPickService, selectedAction: ((UIAlertAction) -> Void)?) {
        self.output?.didAddPhoto(withPicker: picker, selectedAction: selectedAction)
    }
    
}
 
 extension MakePostViewModel: AudioDescriptionCellOutput {
  
    func showAlert(_ message: String) {
    }

    func openAudioActionsMenu(actions: [(String, UIAlertAction.Style)], selectedAction: @escaping (UIAlertAction) -> Void) {
        output?.openAudioActionsMenu(actions: actions, selectedAction: selectedAction)
    }

    func openAudioRecorder(with descriptionObject: AudioDescriptionCellObject) {
        self.output?.openAudioRecorder(with: descriptionObject)
    }
 }
 
extension MakePostViewModel {

    func createPost() {

        let titleCellObj  = makePostContentConfig.sections[0].cell as! DescriptionWithPlaceholderCellObject
        let photosHeader  = makePostContentConfig.sections[0].header as! HeaderWithCollectionDisplayModel
        let noteCellObj   = makePostContentConfig.sections[1].cell as! NotateCellObject
        let audioDescObj  = makePostContentConfig.sections[2].cell as! AudioDescriptionCellObject
        let confLevelObj  = makePostContentConfig.sections[3].cell as! ConfLevelCellObject

        var photos = [PhotoForUploadObject]()

        for img in photosHeader.images {
            photos.append(img as! PhotoForUploadObject)
        }

        
        makePostModel.title = titleCellObj.text
        makePostModel.photos = photos
        makePostModel.note = noteCellObj.text
        makePostModel.audioDescription = audioDescObj
        makePostModel.confLevel = confLevelObj.privacyLevel

        dataInput.sendPost(makePostModel)
    }
 }

 extension MakePostViewModel: NotateCellOutput {
    
    func changeSizeOfCell(_ size: CGFloat) {
        expandingCellHeight = size
        output?.updateTableSize()
    }
}


extension NSString {
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
 }
