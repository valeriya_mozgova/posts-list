//
//  MakePostContentConfiguration.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation
import UIKit

struct MakePostContentConfiguration {

    var sections: [MakePostSectionObject]
    
    static func makePostConfig(_ privacyLevel: PrivacyLevel) -> MakePostContentConfiguration {
        
        return MakePostContentConfiguration(sections: sectionsArray(privacyLevel))
    }

    private static func sectionsArray(_ privacy: PrivacyLevel) -> [MakePostSectionObject] {
        
        let section1 = MakePostSectionObject(headerHeight: 100, cell:  DescriptionWithPlaceholderCellObject( title: "Title Post", placeholderText: "Post Name", text: "", enabledTextField: true), header: HeaderWithCollectionDisplayModel(images: [PhotoForUploadObject](), headerSource: .makePost))
        
        let section2 = MakePostSectionObject(headerHeight: 26, cell: NotateCellObject(title: "Note", text: ""), header: nil)
        let section3 = MakePostSectionObject(headerHeight: 26, cell: AudioDescriptionCellObject(isFilled: false, pathToTrack: "", audioStatus: .stopped, audioDurationTime: 0.0, title: "", postId: "", shouldShowMenu: true), header: nil)
        let section4 = MakePostSectionObject(headerHeight: 26, cell: ConfLevelCellObject(privacyLevel: privacy), header: nil)
        
        return [section1, section2, section3, section4]
    }
}

class MakePostSectionObject {

    var headerHeight: CGFloat
    var cell: MakePostViewModelItem
    var header: MakePostHeaderItem?
    
    init(headerHeight: CGFloat, cell: MakePostViewModelItem, header: MakePostHeaderItem?) {
        self.headerHeight = headerHeight
        self.cell = cell
        self.header = header
    }
}
