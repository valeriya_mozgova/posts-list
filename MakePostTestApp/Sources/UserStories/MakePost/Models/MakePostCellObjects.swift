//
//  MakePostDescriptionCellObject.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/26/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation
import UIKit

class DescriptionWithPlaceholderCellObject {

    var title: String
    var text: String
    var placeholderText: String
    var enabledTextField: Bool
    
    init(title: String, placeholderText: String, text: String, enabledTextField: Bool) {
        self.title = title
        self.text = text
        self.placeholderText = placeholderText
        self.enabledTextField = enabledTextField
    }
}

extension DescriptionWithPlaceholderCellObject: MakePostViewModelItem {
    
    var rowHeight: CGFloat {
        return 44
    }

    var cellType: MakePostViewModelItemType {
        return .cellWithPlaceholder
    }
}


class NotateCellObject: MakePostViewModelItem {
    
    var rowHeight: CGFloat {
        return UITableView.automaticDimension
    }

    var cellType: MakePostViewModelItemType {
        return .notateCell
    }

    var title: String
    var text: String
    
    init(title: String, text: String) {
        self.title = title
        self.text = text
    }
}

class AudioDescriptionCellObject: MakePostViewModelItem {
    
    var rowHeight: CGFloat {
        return 44
    }
 
    var cellType: MakePostViewModelItemType {
        return .audioCell
    }

    var postId: String
    var title: String
    var isFilled: Bool
    var pathToAudioTrack: String 
    var audioStatus: AudioStatus
    var audioDurationTime: TimeInterval
    var shouldShowMenu: Bool
    
    init(isFilled: Bool, pathToTrack: String, audioStatus: AudioStatus, audioDurationTime: TimeInterval, title: String, postId: String, shouldShowMenu: Bool) {
        
        self.postId            = postId
        self.isFilled          = isFilled
        self.pathToAudioTrack  = pathToTrack
        self.audioStatus       = audioStatus
        self.audioDurationTime = audioDurationTime
        self.title             = title
        self.shouldShowMenu    = shouldShowMenu
    }
}

class ConfLevelCellObject: MakePostViewModelItem {
    
    var rowHeight: CGFloat {
        return 60
    }
    
    var privacyLevel: PrivacyLevel
    var isVisiblePublish: Bool? = true

    var cellType: MakePostViewModelItemType {
        return .confidentialityLevel
    }
    
    init(privacyLevel: PrivacyLevel, isVisiblePublish: Bool? = nil) {
        self.privacyLevel = privacyLevel
        self.isVisiblePublish = isVisiblePublish
    }
}

class HeaderWithCollectionDisplayModel: MakePostHeaderItem {
    
    var type: MakePostHeaderItemType {
        return .addPhotosType
    }
    var canMoveItems: Bool = false
    var images: [PhotoItem]
    
    var headerSource: HeaderWithCollectionSourceType
    
    init(images: [PhotoItem], headerSource: HeaderWithCollectionSourceType) {
        self.images       = images
        self.headerSource = headerSource
    }
}

enum HeaderWithCollectionSourceType {
    
    case editPost
    case makePost
    case editTrip
    case createPlace
}
