//
//  MakePostModel.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 4/2/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation
import UIKit

class MakePostModel {
    
    var clientId: String
    var title: String?
    var photos: [PhotoForUploadObject]?
    var note: String?
    var audioDescription: AudioDescriptionCellObject?
    var confLevel: PrivacyLevel
    
    init(clientId: String,
     title: String?,
     photos: [PhotoForUploadObject]?,
     note: String?,
     audioDescription: AudioDescriptionCellObject?,
     confLevel: PrivacyLevel) {
        self.clientId = clientId
        self.title = title
        self.photos = photos
        self.note = note
        self.audioDescription = audioDescription
        self.confLevel = confLevel
    }
}

class PhotoForUploadObject: PhotoItem {
    
    var photoItemType: PhotoItemType {
        return .photoForUploadObject
    }
    
    var postId: String
    var photo: UIImage
    var title: String
    var pathToImage: String
    
    var imageData: Data? {

        return photo.jpegData(compressionQuality: 0.5)
    }
    
    var isEdited: Bool

    
    init(photo: UIImage, title: String, pathToImage: String, postId: String, isEdited: Bool) {
        
        self.photo              = photo
        self.title              = title
        self.pathToImage        = pathToImage
        self.postId             = postId
        self.isEdited           = isEdited
    }
}

class PostDisplayModel {
    
    var itemSubtype: String? {
        return ""
    }
    
    var privacy: String? {
        return privacyLevel
    }
    
    var socialTitleForUrl: String {
        return ""
    }
    
    var stringItemType: String {
        return "POST"
    }
    
    var itemId: String {
        return id
    }
    
    var rowHeight: CGFloat {
        return UITableView.automaticDimension
    }
    
    var id: String
    var privacyLevel: String
    var postDescription: String
    var title: String
    var audioDescription: AudioDescriptionCellObject?
    var photosArray: [PhotoForUploadObject]
    var tripCreationDate: TimeInterval?
    
    init(id: String, privacyLevel: String, photosArray: [PhotoForUploadObject], postDescription: String, title: String, audioDescription: AudioDescriptionCellObject?) {
        
        self.id = id
        self.privacyLevel = privacyLevel
        self.postDescription = postDescription
        self.title = title
        self.photosArray = photosArray
        self.audioDescription = audioDescription
        self.photosArray = photosArray
    }
}
