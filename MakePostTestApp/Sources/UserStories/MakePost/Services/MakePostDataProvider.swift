//
//  MakePostDataProvider.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation

class MakePostDataProvider: MakePostViewModelDataInput {
    func sendPost(_ post: MakePostModel) {
        
        CoreDataManager.shared().saveCreatedPost(post) { (postDisplay) in }
    }
}
