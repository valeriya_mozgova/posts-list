//
//  PhotosPickService.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 6/1/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import PhotosUI

protocol PhotosPickerServiceOutput: class {
    
    func updateHeader(withPhoto photoObject: PhotoForUploadObject)
    func didCancelCamera()
    func showMessage(_ message: String)
}

class PhotosPickService: NSObject {
    
    init(maxSelectionCount: Int) {
        
        self.maxSelectionCount = maxSelectionCount
        super.init()
    }
    
    var maxSelectionCount: Int
    
    weak var output: PhotosPickerServiceOutput?
    
    func saveImageData(data: Data, path: String ) {
        
        let fileManager = FileManager.default
        let tempDir = NSTemporaryDirectory()
        let photosDirectory = URL(fileURLWithPath: tempDir).appendingPathComponent("Photos")
        
        if fileManager.fileExists(atPath: photosDirectory.path) == false {
            do {
                try fileManager.createDirectory(at: photosDirectory, withIntermediateDirectories: false, attributes: nil)
            }
            catch {
                print(error.localizedDescription)
            }
        }
        do {
            try data.write(to: URL(fileURLWithPath: path))
            print(data)
        }
        catch let error {
            print(error.localizedDescription)
        }
    }
    
    func removePhotoPhysically(pathToResource: String, completion: () -> Void) {
        
        if FileManager.default.fileExists(atPath: pathToResource) == true {
            do {
                try FileManager.default.removeItem(at: URL(fileURLWithPath: pathToResource))
            }
            catch {
                print("remove uploaded media for post error: \(error.localizedDescription)")
            }
            completion()
        }
        else {
            print(pathToResource)
        }
    }
    
   private func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    private func downloadImage(url: URL, img: @escaping (UIImage) -> Void){
        
        getDataFromUrl(url: url) { data, response, error in
            
            guard let data = data, error == nil else { return }
        
            DispatchQueue.main.async() {
                if let image = UIImage(data: data) {
                    img(image)
                }
            }
        }
    }
}

extension PhotosPickService: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        var photoObject: PhotoForUploadObject!
        
        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else { return }
        
        let photosDirectory = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("Photos")
        
        let timestamp = Date().timeIntervalSince1970
        
        let timestampInt = Int(timestamp)
        
        let uniqueFileName = "\(timestampInt).JPG"
        let pathToImg = photosDirectory.appendingPathComponent(uniqueFileName)

        photoObject = PhotoForUploadObject(photo: image, title: uniqueFileName, pathToImage: pathToImg.path, postId: "", isEdited: false)
        
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        guard let imgData = photoObject.imageData else { return }
        
        saveImageData(data: imgData, path: photoObject.pathToImage)
        output?.updateHeader(withPhoto: photoObject)

        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: { [weak self] in
            
            self?.output?.didCancelCamera()
        })
    }
}

extension PhotosPickService: AssetsPickerViewControllerDelegate {
    
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        
        for asset in assets {
            
            DispatchQueue.global(qos: .default).async {
                
                if asset.mediaType == .video {
                    self.output?.showMessage("Sorry, you could not attach video")
                    return
                }
                
                let resources = PHAssetResource.assetResources(for: asset)
                let originalFilename = resources[0].originalFilename
                
                let filenameURL = URL(string: originalFilename)
                let pathExt = filenameURL?.pathExtension
                
                var originalFileName = (filenameURL?.deletingPathExtension().relativeString)! + "\(Int(Date().timeIntervalSince1970))"
                
                originalFileName = originalFileName.appendingFormat(".%@", pathExt!)
                
                let image = Helpers.getAssetThumbnail(asset: asset)
                let photosDirectory = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("Photos")
                
                let pathToImg = photosDirectory.appendingPathComponent(originalFileName)
                
                let photoObject = PhotoForUploadObject(photo: image, title: originalFilename, pathToImage: pathToImg.path, postId: "", isEdited: false)
                
                guard let imgData = photoObject.imageData else { return }

                self.saveImageData(data: imgData, path: photoObject.pathToImage)
                
                DispatchQueue.main.async {
                    self.output?.updateHeader(withPhoto: photoObject)
                }
            }
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        if controller.selectedAssets.count > maxSelectionCount {
            // do your job here
            return false
        }
        return true
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
