//
//  MakePostConstants.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/23/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation
import UIKit

let kAudioRecorderStoryboardID = "AudioRecorderVC"
let kHeaderWithCollectionXibName = "MakePostHeaderWithCollection"
let kHeaderWithCollectionReuseID = "MakePostHeaderWithCollection"
let kHeaderWithButtonsXibName = "MakePostHeaderWithButtons"
let kHeaderWithButtonsReuseID = "HeaderWithButtons"
let kBaseHeigtEditPostNoteCell = CGFloat(100)
let kHeigtEditPostNoteCellWithoutTxtView = CGFloat(75)
let kCellBtnLeadinConstraint = CGFloat(19)
