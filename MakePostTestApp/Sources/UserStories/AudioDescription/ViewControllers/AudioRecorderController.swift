//
//  AudioRecorderController.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/29/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit
import SVProgressHUD

class AudioRecorderController: UIViewController {

    // outlets
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var startRecordBtn: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    
    // properties
    weak var output: AudioRecorderControllerOutput?
    
    lazy var audioRecorderService: AudioRecorderService = {
        return AudioRecorderService()
    }()
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0.82
        AudioManager.shared().startAudioSession()
        audioRecorderService.setupRecorder()
        audioRecorderService.output = self
        stopBtn.isEnabled = false
    }
    
    // MARK: Public
    func configure(with audioDescription: AudioDescriptionCellObject) {
        audioRecorderService.fillAudioDescription(audioDescription)
    }
    
    // MARK: Actions
    @IBAction func didStartRecording(_ sender: UIButton) {
        
        if AudioManager.shared().appHasMicPermission {
            if !sender.isSelected {
                audioRecorderService.record()
                startRecordBtn.setImage(UIImage(named: "pauseAudio"), for: .normal)
            }
            else {
                audioRecorderService.pauseRecord()
                startRecordBtn.setImage(UIImage(named: "record"), for: .normal)
            }
            
            sender.isSelected = !sender.isSelected
            stopBtn.isEnabled = true
        }
        else {
            self.showAlert(title: "Microphone Access Denied", message: "This app requires access to your device's Microphone.\n\nPlease enable Microphone access for this app in Settings / Privacy / Microphone", actions: [("Cancel", .cancel), ("Settings", .default)], style: .alert) { (action) in
                switch action.title {
                case "Cancel":
                    self.dismiss(animated: true, completion: nil)
                case "Settings":
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                default: break
                }
            }
        }
    }
    
    @IBAction func didStopRecording(_ sender: UIButton) {
      
        audioRecorderService.stopRecording()
        
        self.dismiss(animated: true) {
            self.output?.didSaveRecordedAudio(self.audioRecorderService.audioDescription)
            self.output?.removeBlurredBackgroundView()
        }
    }
    
    @IBAction func didCancelRecording(_ sender: UIButton) {
        
        if audioRecorderService.audioRecorder.currentTime > 0 {
            
            pauseRecorder()
            
            showAlert(title: "Record you made was not saved", actions: [("Leave", .default), ("Back to recording", .default)], style: .alert) { (action) in
                
                switch action.title {
                case "Leave":
                    self.audioRecorderService.removeAudioDescription()
                    self.dismiss(animated: true, completion: {
                        self.output?.removeBlurredBackgroundView()
                    })
                case "Back to recording":
                    break
                default:
                    break
                }
            }
        }
        else {
            self.dismiss(animated: true, completion: {
                self.output?.removeBlurredBackgroundView()
            })
        }
    }
    
    //MARK: Private
    
    private func resetRecorder() {
        
        timeLabel.text = "00:00"
        startRecordBtn.setImage(#imageLiteral(resourceName: "record"), for: .normal)
        startRecordBtn.isSelected = false
        stopBtn.isEnabled = false
        
    }
    
    private func pauseRecorder() {
        
        self.audioRecorderService.pauseRecord()
        startRecordBtn.setImage(#imageLiteral(resourceName: "stop"), for: .normal)
        startRecordBtn.isSelected = false
    }
}

extension AudioRecorderController: AudioRecorderServiceOutput {
    
    func didRecieveSavingError(_ error: String) {
        showAlert(message: error, style: .alert)
    }

    func updateCurrentTime(_ time: String) {
        timeLabel.text = time
    }
}
