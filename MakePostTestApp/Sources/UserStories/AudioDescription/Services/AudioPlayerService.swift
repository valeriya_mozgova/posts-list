//
//  AudioPlayerService.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/29/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit
import AVKit

class AudioPlayerService: NSObject {

    // properties
    var audioPlayer: AVAudioPlayer!
    var audioDescription: AudioDescriptionCellObject!
    weak var output: AudioPlayerServiceOutput?
    fileprivate var updateTimer: CADisplayLink!
    
    init(audioDescription: AudioDescriptionCellObject, output: AudioPlayerServiceOutput) {
        super.init()
        
        self.audioDescription = audioDescription
        self.output           = output
        
        do {
             try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.spokenAudio, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        }
        catch {
            print(error.localizedDescription)
        }    
    }
    
    func playRecord() {
        
        if audioPlayer != nil {
            if audioDescription.audioStatus == .playing {
                if audioPlayer.duration > 0.0 {
                    startUpdateLoop()
                    audioPlayer.play()
                }
            }
            else {
                stopUpdateLoop()
                stopPlaying()
            }
        }
    }
    
    func stopPlaying() {
        if audioPlayer != nil {
            audioPlayer.stop()
                stopUpdateLoop()
            }
        }
    
    func startUpdateLoop() {
        if updateTimer != nil {
            updateTimer.invalidate()
        }
        
        updateTimer = CADisplayLink(target: self, selector: #selector(updateLoop))
        updateTimer.preferredFramesPerSecond = 15;
        updateTimer.add(to: RunLoop.current, forMode: RunLoop.Mode.default)
    }
    
    func stopUpdateLoop() {
        
        if updateTimer != nil {
            updateTimer.invalidate()
            updateTimer = nil
        }
    }
    
    @objc func updateLoop() {
        
        if audioDescription.audioStatus == .playing {
            audioDescription.audioDurationTime = audioPlayer.duration
            self.output?.updatePlayerUI(audioDescription, playingTime: audioPlayer.currentTime)
        }
    }
    
    func setupPlayer() {
        
        self.audioPlayer = nil
        
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Helpers.libraryPath(self.audioDescription.pathToAudioTrack)!))
            
            self.audioPlayer.delegate = self
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    deinit {
        
    }
}

extension AudioPlayerService: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    
        audioDescription.audioStatus = .stopped
        self.output?.updatePlayerUI(audioDescription, playingTime: audioPlayer.currentTime)
    }
}
