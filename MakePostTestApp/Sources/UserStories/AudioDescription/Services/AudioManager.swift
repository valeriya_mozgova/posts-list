//
//  AudioManager.swift
//  AVFoundationTest
//
//  Created by Lera Mozgovayaon 3/28/18.
//  Copyright © 2018 Lera Mozgovaya. All rights reserved.
//

import UIKit
import AVFoundation

class AudioManager: NSObject {

    private static var instance: AudioManager!
    
    var appHasMicPermission: Bool = false
    
    let audioSession = AVAudioSession.sharedInstance()

    static func shared() -> AudioManager {
        if instance == nil {
            instance = AudioManager()
        }
        return instance
    }
    
    func startAudioSession() {
        do {
            
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: .spokenAudio, options: .defaultToSpeaker)
            
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            
            audioSession.requestRecordPermission({ (granted) in
                self.appHasMicPermission = granted
            })
        }
        catch {
            print("Audio session configuration problem: \(error.localizedDescription)")
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
