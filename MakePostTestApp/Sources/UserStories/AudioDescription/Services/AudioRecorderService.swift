//
//  AudioRecorderService.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 3/29/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit
import AVFoundation

class AudioRecorderService: NSObject {

    var audioRecorder: AVAudioRecorder!
    var audioDescription: AudioDescriptionCellObject!
    weak var output: AudioRecorderServiceOutput?
    
    fileprivate var updateTimer: CADisplayLink!
    
    func fillAudioDescription(_ audioDescription: AudioDescriptionCellObject) {
         self.audioDescription = audioDescription
    }
    
    //MARK: Recording
    func setupRecorder() {
        
        audioDescription.pathToAudioTrack = getURLforMemo().path
        
        let recordSettings = [AVFormatIDKey: Int( kAudioFormatMPEG4AAC),
                              AVSampleRateKey: 44100,
                              AVNumberOfChannelsKey: 1,
                              AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
        
        do {
            audioRecorder = try AVAudioRecorder(url: URL(string: audioDescription.pathToAudioTrack)! , settings: recordSettings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        }
        catch let error {
            print(error.localizedDescription)
        }
    }
    
    func record() {
        audioDescription.audioStatus = .recording
        audioRecorder.record(forDuration: 60)
        startUpdateLoop()
    }
    
    func pauseRecord() {
        audioDescription.audioStatus = .paused
        audioRecorder.pause()
    }
    
    func stopRecording() {
        audioDescription.audioDurationTime = audioRecorder.currentTime
        
        audioRecorder.stop()
        audioDescription.audioStatus = .stopped
        audioDescription.isFilled = true
        stopUpdateLoop()
    }
    
    // MARK: - Helpers
    private func getURLforMemo() -> URL {
        
        let fileManager = FileManager.default
        let tempDir = NSTemporaryDirectory()
        let memosDirectory = URL(fileURLWithPath: tempDir).appendingPathComponent("Memos")
        
        var filePath = ""

        if fileManager.fileExists(atPath: memosDirectory.path) == false {
            do {
                try fileManager.createDirectory(at: memosDirectory, withIntermediateDirectories: false, attributes: nil)
            }
            catch let error {
                print(error.localizedDescription)
            }
        }
        
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: memosDirectory, includingPropertiesForKeys: nil)
            // process files
            filePath = memosDirectory.appendingPathComponent("\(fileURLs.count )" + "\(Date().timeIntervalSince1970)").appendingPathExtension("mp4").path
            for path in fileURLs {
                print(path)
            }
        } catch {
            print("Error while enumerating files \(memosDirectory.path): \(error.localizedDescription)")
        }
        
        return URL(fileURLWithPath: filePath)
    }
    
    // MARK: - Update Loop
   private func startUpdateLoop() {
        if updateTimer != nil {
            updateTimer.invalidate()
        }
        
        updateTimer = CADisplayLink(target: self, selector: #selector(updateLoop))
        updateTimer.preferredFramesPerSecond = 15;
        updateTimer.add(to: RunLoop.current, forMode: RunLoop.Mode.default)
    }
    
   fileprivate func stopUpdateLoop() {
        
        if updateTimer != nil {
            updateTimer.invalidate()
            updateTimer = nil
        }
    }
    
  @objc private func updateLoop() {
        
        if audioDescription.audioStatus == .recording {
            self.output?.updateCurrentTime(audioRecorder.currentTime.stringTimeSec)
        }
    }
    
    //MARK: Deleting
    func removeAudioDescription() {
        do {
            let path = audioDescription.pathToAudioTrack
            try FileManager.default.removeItem(atPath: path)
        }
        catch let error {
            self.output?.didRecieveSavingError(error.localizedDescription)
        }
    }
}

extension AudioRecorderService: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if flag == true {
            audioDescription.audioStatus = .stopped
            stopUpdateLoop()
        }
    }
}
