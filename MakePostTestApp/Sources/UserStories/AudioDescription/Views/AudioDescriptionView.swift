//
//  AudioDescriptionView.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 12/21/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit
import SVProgressHUD
import AVFoundation

class AudioDescriptionView: UIView {

    //outlets
    @IBOutlet weak var filledAudioDescriptionView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var menuBtnWidth: NSLayoutConstraint!
    
    @IBOutlet weak var microphoneBtn: UIButton!
    @IBOutlet var contentView: UIView!
    
    lazy var audioPlayer: AudioPlayerService = {
        
        return AudioPlayerService(audioDescription: audioDescriptionObject, output: self)
    }()
    
    //properties
    weak var output: AudioDescriptionCellOutput?
    
    var audioDescriptionObject: AudioDescriptionCellObject! {
        didSet {
            
            if audioDescriptionObject.isFilled == true {
                audioPlayer.setupPlayer()
            }
            configure()
        }
    }
    
   private func configure() {

        filledAudioDescriptionView.isHidden = !audioDescriptionObject.isFilled
        timeLabel.text = audioDescriptionObject.audioDurationTime.stringTimeSec
        microphoneBtn.isEnabled = !audioDescriptionObject.isFilled

        if audioDescriptionObject.shouldShowMenu == true {
            menuBtnWidth.constant = 44
        }
        else {
            menuBtnWidth.constant = 0
        }
    }

    func updateUIWhilePlaying(_ audioDescription: AudioDescriptionCellObject, playingTime: TimeInterval) {
        
        let progressValue = playingTime / audioDescription.audioDurationTime
        
        if audioDescription.audioStatus == .playing {
            
            timeLabel.text = playingTime.stringTimeSec
            progressView.setProgress(Float(progressValue), animated: true)
        }
        else {
            timeLabel.text = audioDescription.audioDurationTime.stringTimeSec
            progressView.setProgress(0, animated: false)
        }
    }
    
    //MARK: Actions
    @IBAction func didClickMicrophone(_ sender: UIButton) {
        self.output?.openAudioRecorder(with: audioDescriptionObject)
    }
    
    @IBAction func didClickPlay(_ sender: UIButton) {
        
        switch audioDescriptionObject.audioStatus {
        case .playing:
            audioDescriptionObject.audioStatus = .paused
        case .paused:
            audioDescriptionObject.audioStatus = .playing
        case .stopped:
            audioDescriptionObject.audioStatus = .playing
        case .recording:
            break
        }
  
        audioPlayer.playRecord()
        updatePlayButton()
    }
    
    @IBAction func didClickMenu(_ sender: UIButton) {
        
        let alertActions: [(String, UIAlertAction.Style)] = [("Record New", .default),
                            ("Delete", .destructive),
                            ("Cancel", .cancel)]
        
        self.output?.openAudioActionsMenu(actions: alertActions, selectedAction: { [weak self] action in
            
            switch action.title {
            case "Record New":
                self?.removeAudioDescription({ (removed) in
                    self?.output?.openAudioRecorder(with: (self?.audioDescriptionObject)!)
                })
                
                self?.audioPlayer.audioPlayer = nil

            case "Delete":
                self?.removeAudioDescription() { (_) in  }
                
                self?.audioPlayer.audioPlayer = nil

            case "Cancel": break
            default: break
            }
        })
    }
    
    fileprivate func updateAudioDescriptionObject() {
        audioDescriptionObject.isFilled = false
        audioDescriptionObject.pathToAudioTrack = ""
        audioDescriptionObject.shouldShowMenu = false
        audioDescriptionObject.audioDurationTime = 0
        audioDescriptionObject.audioStatus = .stopped
    }
    
    func removeAudioDescription(_ completion: ((AudioDescriptionCellObject) -> Void)? = nil) {
        
            do {
                let path = audioDescriptionObject.pathToAudioTrack
                
                if path != "" {
                    try FileManager.default.removeItem(atPath: path)
                    
                    updateAudioDescriptionObject()
                    
                    configure()
                    
                    if let compl = completion {
                        compl(audioDescriptionObject)
                    }
                }
            }
            catch {
                output?.showAlert(error.localizedDescription)
            }
    }

    private func updatePlayButton() {
        let img = (audioDescriptionObject.audioStatus == .playing) ? UIImage(named: "pauseAudio") : #imageLiteral(resourceName: "triangle5")
        playBtn.setImage(img, for: .normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commontInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commontInit()
    }
    
    private func commontInit() {
        
        Bundle.main.loadNibNamed(String(describing: AudioDescriptionView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}

extension AudioDescriptionView: AudioPlayerServiceOutput {
    
    func updatePlayerUI(_ audioDescription: AudioDescriptionCellObject, playingTime: TimeInterval) {
        
        updateUIWhilePlaying(audioDescription, playingTime: playingTime)
        updatePlayButton()
    }
}

