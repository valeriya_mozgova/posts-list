//
//  ListViewController.swift
//  MakePostTestApp
//
//  Created by Lera Mozgovaya on 2/14/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var noContentLabel: UILabel!
    
    lazy var viewModel: ListViewModel = {
        return ListViewModel(dataInput: self.dataProvider)
    }()
    
    lazy var dataProvider: PostsDataProvider = {
        return PostsDataProvider()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binding()

        viewModel.reloadDataClosure = { [weak self] contentCount in
            if contentCount > 0 {
                self?.tableView.reloadData()
                self?.tableView.isHidden = false
            }
            else {
                self?.tableView.isHidden = true
                self?.noContentLabel.isHidden = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.fetchPosts()
    }
    
    @IBAction func didClickAddPost(_ sender: UIBarButtonItem) {
        
        let vc: MakePostViewController = MakePostViewController.instantiate(appStoryboard: .makePost)
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func binding() {
        tableView.delegate = viewModel
        tableView.dataSource = viewModel
    }
}
