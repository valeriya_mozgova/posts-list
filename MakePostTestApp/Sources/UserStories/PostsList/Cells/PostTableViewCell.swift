//
//  TripViewTableViewCell.swift
//  Enguide
//
//  Created by Chaban Nikolay on 1/12/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class PostTableViewCell: BaseTableViewCell {

    // outlets
    @IBOutlet weak var photosCarouselView: PhotosCarouselView!
    @IBOutlet weak var namePlaceLabel: UILabel!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
   
    @IBOutlet weak var baseInfoStackView: UIStackView!
    
    @IBOutlet weak var audioDescriptionView: AudioDescriptionView!
    @IBOutlet weak var commonImagesView: UIView!


    var postDisplayModel: PostDisplayModel!
    
    //MARK: - Public
    func configurePostCell(_ post: PostDisplayModel, isEditingState: Bool) {
        
        postDisplayModel = post
        
        setupCommon(post)
    }
    
    //MARK: Private func
    
    private func setupCommon(_ post: PostDisplayModel) {
        
        self.descriptionTitleLabel.isHidden = post.postDescription.isEmpty
        self.descriptionTitleLabel.text     = post.postDescription
        
        self.namePlaceLabel.text = post.title
        
        var imagesPost = [String]()
        
        if post.photosArray.isEmpty == false {
            
            photosCarouselView.shouldBlurBackground = true
            for photo in post.photosArray {
                imagesPost.append(Helpers.libraryPath(photo.pathToImage) ?? "")
            }
        }
        else {
            photosCarouselView.shouldBlurBackground = false
            imagesPost.append("")
        }

        photosCarouselView.configureWithPhotosPaths(imagesPost)
        
        if let audioDesc = post.audioDescription {
            audioDescriptionView.audioDescriptionObject = audioDesc
            audioDescriptionView.isHidden = false
        }
        else {
            audioDescriptionView.isHidden = true
        }
    }
}

