//
//  ListViewModel.swift
//  MakePostTestApp
//
//  Created by Lera Mozgovaya on 2/14/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

class ListViewModel: NSObject {

    var reloadDataClosure: ((Int) -> Void)?
    weak var dataInput: PostsDataInput?
    
    private var content = [PostDisplayModel]() {
        didSet {
            reloadDataClosure?(self.content.count)
        }
    }
    
    init(dataInput: PostsDataInput) {
        self.dataInput = dataInput
    }
    
    func fetchPosts() {
        dataInput?.fetchPosts { posts in
            self.content = posts
        }
    }
}

extension ListViewModel: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = PostTableViewCell.cell(tableView: tableView) as? PostTableViewCell
            else { return UITableViewCell() }
    
        let post = content[indexPath.row]
        cell.configurePostCell(post, isEditingState: false)
        
        return cell
    }
}

extension ListViewModel: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
