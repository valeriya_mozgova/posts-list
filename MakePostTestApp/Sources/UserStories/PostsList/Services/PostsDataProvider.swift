//
//  PostsDataProvider.swift
//  MakePostTestApp
//
//  Created by Lera Mozgovaya on 2/14/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

protocol PostsDataInput: class {
    func fetchPosts(_ posts: ([PostDisplayModel]) -> Void)
}

class PostsDataProvider: PostsDataInput {
    
    func fetchPosts(_ posts: ([PostDisplayModel]) -> Void) {
        posts(postsForDisplaying())
    }
    
    private func postsForDisplaying() -> [PostDisplayModel] {
        
        var displayingPosts = [PostDisplayModel]()
        
        if let posts = CoreDataManager.shared().fetchPosts() {
            posts.forEach { (en) in
                displayingPosts.append(CoreDataManager.shared().postDisplayModel(en))
            }
        }
        
        return displayingPosts
    }
}
