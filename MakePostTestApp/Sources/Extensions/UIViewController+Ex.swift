//
//  UIViewController+Alerts.swift
//  MakePostTestApp
//
//  Created by Lera Mozgovaya on 2/14/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(title: String? = nil, message: String? = nil, actions: [(String, UIAlertAction.Style)]? = nil, style: UIAlertController.Style, completion: ((UIAlertAction) -> Void)? = nil) {

        let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: style)
        
        if let actions = actions {
            for i in 0..<actions.count {
                let action = UIAlertAction(title: actions[i].0, style: actions[i].1, handler: completion)
                actionSheetController.addAction(action)
            }
        }

        present(actionSheetController, animated: true, completion: nil)
    }
    
    class func instantiate<T: UIViewController>(appStoryboard: AppStoryboard) -> T {
        
        let storyboard = UIStoryboard(name: appStoryboard.rawValue, bundle: nil)
        let identifier = String(describing: self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! T
    }
}


