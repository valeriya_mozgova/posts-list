//
//  UITextView+Ex.swift
//  Enguide
//
//  Created by OnSightTeam on 10/16/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import Foundation


import UIKit

extension UITextView {
    
    func newHeight(withBaseHeight baseHeight: CGFloat) -> CGFloat {
        
        // Calculate the required size of the textField
        let fixedWidth = frame.size.width
        let newSize = sizeThatFits(CGSize(width: fixedWidth, height: .greatestFiniteMagnitude))
        var newFrame = frame
        
        // Height is always >= the base height, so calculate the possible new height
        let height: CGFloat = newSize.height > baseHeight ? newSize.height : baseHeight
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: height)
        
        return newFrame.height
    }
}
