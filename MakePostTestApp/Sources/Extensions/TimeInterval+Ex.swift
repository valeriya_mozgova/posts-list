//
//  TimeInterval+Ex.swift
//  MakePostTestApp
//
//  Created by Lera Mozgovaya on 2/14/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    private var milliseconds: Int {
        return Int((truncatingRemainder(dividingBy: 1)) * 1000)
    }
    
    private var seconds: Int {
        return Int(self) % 60
    }
    
    private var minutes: Int {
        return (Int(self) / 60 ) % 60
    }
    
    var stringTime: String {
        return String(format: "%0.2d:%0.2d.%0.3d", minutes, seconds, milliseconds)
    }
    
    var stringTimeSec: String {
        return String(format: "%0.2d:%0.2d", minutes, seconds)
    }
}
