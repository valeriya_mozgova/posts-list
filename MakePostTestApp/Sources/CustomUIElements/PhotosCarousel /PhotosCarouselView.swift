//
//  PhotosCarouselScrollView.swift
//  Enguide
//
//  Created by Lera Mozgovaya on 12/17/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class PhotosCarouselView: UIView {

    @IBOutlet weak var pageControl: OSPageControl!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate   = self
            collectionView.dataSource = self
        }
    }
    
    var shouldBlurBackground: Bool = false
    
    var didTapPhoto: ((Int) -> Void)?
    
    var canTapOnPhoto: Bool = false {
        didSet {
            if canTapOnPhoto == true {
                addTap()
            }
        }
    }
    
    private var content = [String]() {
        didSet {
            pageControl.isHidden = content.count <= 1
            pageControl.numberOfPages = content.count
        }
    }
    
    var selectPhotoPath: String?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commontInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commontInit()
    }
    
    private func commontInit() {
        
        Bundle.main.loadNibNamed(String(describing: PhotosCarouselView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    public func configureWithPhotosPaths(_ photoPathsArray: [String]) {
        content = photoPathsArray
        collectionView.reloadData()
    }
    
    func removePhotoAtIndx(_ idx: Int) {
        content.remove(at: idx)
        collectionView.reloadData()
    }
    
    func replacePhoto(atIndex index: Int, withImg image: UIImage) {
        
        //Append content by image at Index
    }
    
    func addTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        collectionView.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        if let tap = didTapPhoto {
            tap(pageControl.currentPage)
        }
    }
}

extension PhotosCarouselView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
}

extension PhotosCarouselView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return content.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = PhotoCarouselCollectionViewCell.cell(collectionView: collectionView, indexPath: indexPath) as? PhotoCarouselCollectionViewCell else {
            return UICollectionViewCell() }
    
        cell.configure(withImgPath: content[indexPath.row], shouldBlurBackground: shouldBlurBackground)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row <= content.count {
            pageControl.currentPage = indexPath.row
            selectPhotoPath = content[indexPath.row]
        }
    }
}
