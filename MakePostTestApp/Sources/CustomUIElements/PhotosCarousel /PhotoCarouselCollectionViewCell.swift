//
//  PhotoCarouselCollectionViewCell.swift
//  Enguide
//
//  Created by Lera Mozgovaya on 12/21/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import UIKit

class PhotoCarouselCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var rearImageView: UIImageView!
    @IBOutlet weak var blurredView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(withImgPath path: String, shouldBlurBackground: Bool) {
        if shouldBlurBackground {
            blurredView.isHidden = false
            fillImageView(imgView, isFrontImg: true, with: path)
            fillImageView(rearImageView, isFrontImg: false, with: path)
        }
        else {
            blurredView.isHidden = true
            fillImageView(imgView, isFrontImg: false, with: path)
        }
    }
    
    private func fillImageView(_ imageView: UIImageView, isFrontImg: Bool, with path: String) {
        
        let appliedImgContentMode: UIView.ContentMode = isFrontImg ? .scaleAspectFit : .scaleAspectFill
        
        if path.contains(NSTemporaryDirectory()) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                imageView.image = UIImage(data: data)
                imageView.backgroundColor = .clear
                imageView.contentMode = appliedImgContentMode
            }
            catch {
                imageView.image = UIImage(named: "Placeholder")
                imageView.backgroundColor = UIColor(white: 0.8, alpha: 0.2)
                imageView.contentMode = .center
            }
        }
        else if path == "" {
            imageView.image = UIImage(named: "Placeholder")
            imageView.backgroundColor = UIColor(white: 0.8, alpha: 0.2)
            imageView.contentMode = .center
        }
    }
}
