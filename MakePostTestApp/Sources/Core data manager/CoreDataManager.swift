//
//  CoreDataManager.swift
//  Enguide
//
//  Created by Lera Mozgovayaon 11/16/17.
//  Copyright © 2017 Traveleeg Inc. All rights reserved.
//

import UIKit
import CoreData

@objc class CoreDataManager: NSObject {

    private let containerName = "MakePostTestApp"

    // MARK: Init
    
    private static var instance: CoreDataManager!
    private override init() {}
    
    @objc class func shared() -> CoreDataManager {
        if instance == nil {
            instance = CoreDataManager()
        }
        return instance!
    }
    
    // MARK: - Core Data Stack

    lazy var managedObjectContext: NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()

    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.containerName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {

        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

