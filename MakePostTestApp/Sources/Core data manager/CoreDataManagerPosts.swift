//
//  CoreDataManagerPosts.swift
//  Enguide
//
//  Created by Lera Mozgovaya on 1/15/18.
//  Copyright © 2018 Traveleeg Inc. All rights reserved.
//

import CoreData
import UIKit

extension CoreDataManager {
    
    //MARK: Create

    func saveCreatedPost(_ model: MakePostModel, completion: (PostDisplayModel) -> Void) {
        
        let postEntity: PostEntity = PostEntity(context: managedObjectContext)
        
        postEntity.title = model.title
        postEntity.postDescription = model.note
        
        postEntity.id = model.clientId
    
        if let photos = model.photos {
            if photos.count > 0 {
                for photo in photos {
                    savePhoto(photo) { (savePhoto) in
                        savePhoto.post = postEntity
                    }
                }
            }
        }
        
        if let audioDesc = model.audioDescription, audioDesc.audioDurationTime > 0 {
            
            let audioDescriptionEntity = AudioDescriptionEntity(context: managedObjectContext)
            
            audioDescriptionEntity.durationTime = audioDesc.audioDurationTime
            let pathToAudio = model.audioDescription?.pathToAudioTrack ?? ""
            audioDescriptionEntity.pathToMemo = Helpers.removeHomeDirectoryPath(pathToAudio)
            
            audioDescriptionEntity.title = audioDesc.title
            postEntity.audioDescription = audioDescriptionEntity
        }
        
        postEntity.privacyLevel = model.confLevel.privacyLevel

        saveContext()

        completion(postDisplayModel(postEntity))
    }
    
    func savePhoto(_ model: PhotoForUploadObject, savedPhoto: (PhotoForUploadEntity) -> Void) {
        
        let photoEntity: PhotoForUploadEntity = PhotoForUploadEntity(context: managedObjectContext)
    
        photoEntity.localPath = Helpers.removeHomeDirectoryPath(model.pathToImage)
        photoEntity.title = model.title
        photoEntity.isEdited = model.isEdited
        
        savedPhoto(photoEntity)
        saveContext()
    }
    
    
    //MARK: Fetch
    
    func audioDescriptionEntity(byPostId postId: String) -> AudioDescriptionEntity? {
        
        let fetchRequest: NSFetchRequest<AudioDescriptionEntity> = AudioDescriptionEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "%K = %@", #keyPath(AudioDescriptionEntity.post.id), postId)
        
        do {
            let result = try self.managedObjectContext.fetch(fetchRequest) as [AudioDescriptionEntity]
            if result.count > 0 {
                return result.first
            }
            else { return nil }
        }
        catch { return nil }
    }
  
    func postEntity(id: String) -> PostEntity? {
        
        let fetchRequest: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "%K = %@", #keyPath(PostEntity.id), id)
        
        do {
            let result = try self.managedObjectContext.fetch(fetchRequest) as [PostEntity]
            if result.count > 0 {
                return result.first
            }
            else { return nil }
        }
        catch { return nil }
    }

    func photoForUploadEntity(id: String) -> PhotoForUploadEntity? {
        let fetchRequest: NSFetchRequest<PhotoForUploadEntity> = PhotoForUploadEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "%K = %@", #keyPath(PhotoForUploadEntity.title), id)
        
        do {
            let result = try self.managedObjectContext.fetch(fetchRequest) as [PhotoForUploadEntity]
            if result.count > 0 {
                return result.first
            }
            else { return nil }
        }
        catch { return nil }
    }
    
    func audioDescriptionEntity(title: String) -> AudioDescriptionEntity? {
        let fetchRequest: NSFetchRequest<AudioDescriptionEntity> = AudioDescriptionEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "%K = %@", #keyPath(AudioDescriptionEntity.title), title)
        
        do {
            let result = try self.managedObjectContext.fetch(fetchRequest) as [AudioDescriptionEntity]
            if result.count > 0 {
                return result.first
            }
            else { return nil }
        }
        catch { return nil }
    }

    
    func postDisplayModel(_ post: PostEntity) -> PostDisplayModel {

        var photosForUpload = [PhotoForUploadObject]()
        
        if let photosForUp = post.photos, photosForUp.count > 0 {
            
            for photo in photosForUp {
                
                if let photoEn = photo as? PhotoForUploadEntity {
                    if let photoObj = photoObjectFromUploadPhoto(photoEn)  {
                        photosForUpload.append(photoObj)
                    }
                }
            }
        }
        
        let audioDescription = post.audioDescription != nil ? audioDescriptionDisplay(post.audioDescription!) : nil
   
        let postDisplayModel = PostDisplayModel(id: post.id ?? "",
                                                privacyLevel: post.privacyLevel ?? "",
                                                photosArray: photosForUpload,
                                                postDescription: post.postDescription ?? "",
                                                title: post.title ?? "",
                                                audioDescription: audioDescription)

        return postDisplayModel
    }
    
    func audioDescriptionDisplay(_ audioDescrEntity: AudioDescriptionEntity) -> AudioDescriptionCellObject? {
        
        let audioDesc = AudioDescriptionCellObject(isFilled: true, pathToTrack: audioDescrEntity.pathToMemo ?? "", audioStatus: .stopped, audioDurationTime: audioDescrEntity.durationTime, title: audioDescrEntity.title ?? "", postId: audioDescrEntity.post?.id ?? "", shouldShowMenu: false)
        
        return audioDesc
    }

    func photoObjectFromUploadPhoto(_ photo: PhotoForUploadEntity) -> PhotoForUploadObject? {

        let imgPath = Helpers.libraryPath(photo.localPath!)
        
        var data = Data()
        
        do {
            data = try Data(contentsOf: URL(fileURLWithPath: imgPath!))
            
            let image = UIImage(data: data)
          
            guard let exImage = image else { return nil }
            
            let photoForUpload = PhotoForUploadObject(photo: exImage, title: photo.title ?? "", pathToImage: photo.localPath ?? "", postId: photo.post?.id ?? "", isEdited: photo.isEdited)
            
            return photoForUpload
        }
        catch {
            print(error.localizedDescription)
            
            removePhotoForUpload(photo.title!, completion: {})
            
            return nil
        }
    }

    func fetchPosts() -> [PostEntity]? {
        
        let fetchRequest: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
        
        do {
            let result = try self.managedObjectContext.fetch(fetchRequest) as [PostEntity]
            if result.count > 0 {
                return result
            }
            
        }
        catch { return nil }
        
        return nil
    }
    
    //MARK: Delete
    
    func removeAudioDescriptionObject(_ title: String, completion: () -> Void) {
        
        if let audioDescEntiy = self.audioDescriptionEntity(title: title) {
            self.managedObjectContext.delete(audioDescEntiy)
            saveContext()
            completion()
        }
    }
    
    func removePhotoForUpload(_ photoName: String, completion: () -> Void) {
        
        if let photoForUploadEntity = self.photoForUploadEntity(id: photoName) {
            self.managedObjectContext.delete(photoForUploadEntity)
            saveContext()
            completion()
            
        }
    }
    
    func deletePost(postId: String, completion: () -> Void) {
        
        if let postEntity = self.postEntity(id: postId) {
            managedObjectContext.delete(postEntity)
            saveContext()
            completion()
        }
    }

}
