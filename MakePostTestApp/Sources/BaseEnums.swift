//
//  BaseProtocols.swift
//  MakePostTestApp
//
//  Created by Lera Mozgovaya on 2/14/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import Foundation

enum AppStoryboard: String {
    
    case Main = "Main"
    case audio = "Audio"
    case makePost = "MakePost"
}


